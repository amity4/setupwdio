const Page = require('../page');


class AuthenticationPage extends Page{

    get inputUsername () { return $('input[id="email"]') }
    get inputPassword () { return $('input[id="passwd"]') }
    get btnSignIn () { return $('#SubmitLogin') }
    get btnSingOut() {return $('a[class="logout"]')}

    get txtSignInPageHeader() {return $('h1[class="page-heading"]')}

    get inputCreateAccount() {return $('#email_create')}
    get btnCreateAccount() {return $('#SubmitCreate')}

    async signin (username, password) {
        await this.inputUsername.setValue(username);
        await this.inputPassword.setValue(password);
        await this.btnSignIn.click();
    }

    async createAccount(emailId){
        await this.inputCreateAccount.setValue(emailId);
        await this.btnCreateAccount.click();
    }

    async signOut(){

    }

    async browserPause(){
        console.log('Browser Pause for 5 seconds..!');
        this.browser.pause(5000);
    }

}

module.exports = new AuthenticationPage();