const { getUrl } = require('../page.js');
const Page = require('../page');

class HomePage extends Page {
    /**
     * define selectors using getter methods
     */
    get  signIn() { return $('a[class="login"]') }

    async clickSignin(){
        await this.signIn.click();
    }

    /**
     * overwrite specifc options to adapt it to page object
     */
    async open () {
        console.log('Called from Authentication to HomePage.open')
        return browser.url('http://automationpractice.com/');
    }
}

module.exports = new HomePage();