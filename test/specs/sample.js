const assert = require('assert')

describe('LamdaTest page', () => {
   it('should have the Correct Page title', async () => {
       browser.url('https://www.lambdatest.com/')
       const title = await browser.getTitle()
       console.log('Title Of Page = ' + title)
       assert.equal(title, 'Most Powerful Cross Browser Testing Tool Online | LambdaTest')
   })
})