const { startStep } = require('@wdio/allure-reporter').default
const HomePage = require('../../pageobjects/autopracticepage/home.page')
const AuthenticationPage = require('../../pageobjects/autopracticepage/authentication.page');

describe('Sign in to automation practice application', () => {
    it('should login with valid credentials', async () => {
        startStep("")
        await HomePage.open();
        await HomePage.clickSignin();

        await AuthenticationPage.signin('test-amit@test.com', '123456789');
        console.log('Sign in text header - ' + await AuthenticationPage.txtSignInPageHeader.getText())
        AuthenticationPage.browserPause();
        // await expect(SecurePage.flashAlert).toBeExisting();
        await expect(AuthenticationPage.txtSignInPageHeader).toHaveTextContaining(
            'MY ACCOUNT');
            endStep(status)
    });
});
